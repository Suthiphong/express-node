FROM node:18-alpine
WORKDIR /app

COPY . .

RUN npm i -g pnpm
RUN pnpm i

EXPOSE 8080

CMD ["node", "app.js"]