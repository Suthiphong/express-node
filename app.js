const express = require('express')
const app = express()
const port = 8080

app.get('/info',(req, res) => {
    res.json({
        data: `information -> request -> ${new Date()}`
    })
})

app.get('/', (req, res) => {
    res.json({
        data: `information -> request -> ${new Date()}`
    })
})

app.listen(port, () => console.log(port))
